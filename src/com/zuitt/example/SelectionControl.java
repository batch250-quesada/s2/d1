package com.zuitt.example;
import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {
        //// JAVA OPERATORS ////
        // Arithmetic +, -, *, /, %
        // Comparison >, <, >=, <=, ==, !=
        // Logical &&, ||, !
        // Assignment =

        //// Control Structures in Java ////

        // Short Circuiting //
        int x = 15;
        int y = 0;

        if (y != 0 && x/y == 0) System.out.println(x/y);
        else System.out.println("ok");

        ///Ternary Operator //
        int n = 12;
        Boolean result = (n > 17) ? true : false;
        System.out.println(result);

        //// Switch Case ////
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number: ");

        int direction = scanner.nextInt();

        switch (direction) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("idk");
        }
    }
}
