package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // JAVA COLLECTIONS
    public static void main(String[] args) {
        int [] intArr = new int[3];

        intArr[0] = 1;
        intArr[2] = 3;

        Arrays.sort(intArr);
        System.out.println(Arrays.toString(intArr));

        // Declaration with Initialization
        // Syntax
        // dataType[] identifier = {e1, e2, e3, ...}'

        // different
        // String names[] = {"Chris", "Qiu", "Pogi"};
        // names[2] = "Baby boy";

        String [] names = {"Chris", "Qiu", "Pogi"};
        names[2] = "Baby boy";
        System.out.println(Arrays.toString(names));

        String[][] classroom = new String[3][3];

        // first row
        classroom[0][0] = "chris";
        classroom[0][1] = "mark";
        classroom[0][2] = "cy";

        // second row
        classroom[1][0] = "ysa";
        classroom[1][1] = "mera";
        classroom[1][2] = "maica";

        // third row
        classroom[2][0] = "eson";
        classroom[2][1] = "gabby";
        classroom[2][2] = "rushtin";

        // System.out.println(Arrays.toString(classroom)); // outputs array address only
        System.out.println(Arrays.deepToString(classroom));

        // In Java, the size of the array cannot be modified. If there isa need to add or remove elements, new arrays must be created

        // ARRAYLISTS
        // are resizable arrays, wherein elements can be added or removed whenever it is needed

            // Syntax
            // ArrayList<T> identifier = new ArrayList<T>();
            // <T> is used to specify that the list can only have one type of object in a collection.
            // ArrayList cannot hold primitive data types, "java wrapper class" provides a new way to use these types of data as object.
            // in short, Object version of primitive data types with method.

        // Declaring an ArrayList
        // ArrayList<int> numbers = new ArrayList<int>(); // invalid due to primitive data type, int
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        // Declaring an ArrayList then initialization
        // ArrayList<String> students = new ArrayList<String>();

        // Declaring an ArrayList with values
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("princess bbg", "marceline"));

        // add elements
        // Syntax: arrayListName.add(element);
        students.add("finn");
        students.add("jake");
        System.out.println(students);

        // Access an element
        // arrayListName.get(index);
        System.out.println(students.get(2));

        // Add an element to a specific index.
        // arrayListName.add(index, element)
        students.add(2, "ice king");
        System.out.println(students);
        System.out.println(students.get(2));

        // Updating an element
        // arrayListName.set(index, element);
        students.set(2, "stewie");
        System.out.println(students);
        System.out.println(students.get(2));

        // Removing a specific element
        // arrayListName.remove(index);
        students.remove(2);
        System.out.println(students);

        // Removing all elements;
        // students.clear();
        // System.out.println(students);

        // Getting ArrayList size
        System.out.println(students.size());

        //////// HASHMAPS ////////
        // most objects in Java are defined and are instantiations of Classes that contain a set of properties and methods.

        // syntax
        // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();

        // Declaring HashMaps
        // HashMap<String, String> jobPosition = new HashMap<String, String>();

        // Declaring HashMaps with initialization
        HashMap<String, String> jobPosition = new HashMap<String, String>() {
            {
                put("Teacher", "John");
                put("Artists", "John");
            }
        };

        System.out.println(jobPosition);

        // Add elements
        // hashMapName.put(<fieldName>, <value>);
        jobPosition.put("Student", "chrysa");
        jobPosition.put("Dreamer", "chris");
        jobPosition.put("Dreamer", "qiu");

        System.out.println(jobPosition);

        // Accessing element - we use the field name because they are unique
        // hashmapName.get(<fieldName>);
        System.out.println(jobPosition.get("Student"));
        System.out.println(jobPosition.get("Baby"));

        // Updating hashmap value
        // hashmapName.replace(<fieldNameToChange>, <newFieldValue>);
        jobPosition.replace("Dreamer", "Qiu");
        System.out.println(jobPosition);

        // Removing an element
        // hashmapName.remove(<fieldName>);
        jobPosition.remove("Artists");
        System.out.println(jobPosition);

        // Remove all elements
        // hashmapName.clear();
        // jobPosition.clear();
        // System.out.println(jobPosition);
    }
}
